const { environment } = require('@rails/webpacker')
const less = require('./loaders/less')
const webpack = require('webpack')

environment.loaders.append('less', less)

environment.loaders.get('sass').use.splice(-1, 0, {
  loader: 'resolve-url-loader'
})

environment.plugins.prepend(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    'window.jQuery': 'jquery'
  })
)

module.exports = environment
