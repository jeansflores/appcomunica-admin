every 1.day, at: '02:30 am' do
  runner 'RaffleService.call'
  runner 'ResearchService.finalize'
  runner 'PromotionService.finalize'
end
