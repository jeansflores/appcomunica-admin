require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
require "action_text/engine"
require "action_view/railtie"
# require "action_cable/engine"
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AppComunica
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller
    config.responders.flash_keys = [ :notice, :alert ]

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.time_zone = 'Brasilia'

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default charset: 'utf-8'

    config.action_mailer.smtp_settings = {
      user_name: ENV['MAIL_USERNAME'],
      password: ENV['MAIL_PASSWORD'],
      address: ENV['MAIL_ADDRESS'],
      domain: ENV['MAIL_DOMAIN'],
      port: ENV['MAIL_PORT'],
      authentication: :plain,
      enable_starttls_auto: true
    }

    config.generators.system_tests = nil

    config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    config.i18n.default_locale = 'pt-BR'

    config.paths["app/views"].unshift(Rails.root.join('resources', 'views'))

    config.active_record.belongs_to_required_by_default = false
  end
end
