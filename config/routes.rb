Rails.application.routes.draw do
  devise_for :users, module: 'auth', path: 'auth'

  scope module: 'backend' do
    root to: 'home#index'

    get 'categories/from_research', action: :from_research, controller: :categories
    get 'companies/from_research', action: :from_research, controller: :companies
    get 'companies/link_company_to_research', action: :link_company_to_research, controller: :companies
    get 'me', action: :me, controller: :users
    get 'open_modal', action: :open_modal, controller: :home
    get 'report_researches/report', action: :report, controller: :report_researches
    get 'researches/active_awating', action: :active_awating, controller: :researches
    get 'researches/load_research_participants', action: :load_research_participants, controller: :researches
    patch 'me', action: :update_me, controller: :users
    post 'report_researches/generate-file', action: :generate_file, controller: :report_researches

    resources :ads, except: [:show]
    resources :categories, except: [:show]

    resources :cities
    resources :companies
    resources :countries

    resources :events do
      member do
        post :cancel_publication
        post :publish
      end
    end

    resources :institutes do
      resources :items, except: [:index]
    end

    resources :notifications do
      member do
        post :cancel_publication
        post :publish
      end
    end

    resources :posts do
      member do
        post :cancel_publication
        post :publish
      end
    end

    resources :promotions do
      member do
        post :cancel_publication
        post :publish
      end
    end

    resources :raffles do
      member do
        post :cancel_publication
        post :publish
      end
    end

    resources :researches do
      member do
        get :define_institute
        get :get_research_json
        post :cancel_publication
        post :publish
      end

      resources :sponsors
    end

    resources :report_researches, only: [:index]
    resources :states
    resources :users, except: [:destroy]
  end
end
