class CreateNotification < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.string :title, null: false
      t.string :message, null: false
      t.integer :status, null: false, default: 0
      t.boolean :scheduled, null: false
      t.datetime :schedule, null: true
      t.datetime :published_at

      t.timestamps
    end
  end
end
