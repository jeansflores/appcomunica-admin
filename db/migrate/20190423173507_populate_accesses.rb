class PopulateAccesses < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      INSERT INTO accesses VALUES
        (1, 'Anúncios', 'ads', NOW(), NOW()),
        (2, 'Eventos', 'events', NOW(), NOW()),
        (3, 'Notícias', 'posts', NOW(), NOW()),
        (4, 'Pesquisas', 'researches', NOW(), NOW()),
        (5, 'Promoçãos', 'promotions', NOW(), NOW()),
        (6, 'Sorteios', 'raffles', NOW(), NOW());
    SQL
  end

  def down
    execute <<-SQL
      TRUNCATE TABLE accesses_institutes CASCADE;
      TRUNCATE TABLE accesses CASCADE;
    SQL
  end
end
