class CreateResearches < ActiveRecord::Migration[6.0]
  def change
    create_table :researches do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.string :name, null: false
      t.integer :total_participants, null: false, default: 0
      t.date :start_at, null: false
      t.date :end_at, null: false
      t.integer :status, null: false, default: 0
      t.datetime :published_at

      t.timestamps
    end
  end
end
