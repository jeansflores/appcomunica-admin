class CreateAccessesInstitutes < ActiveRecord::Migration[6.0]
  def change
    create_table :accesses do |t|
      t.string :name, null: false
      t.string :key, null: false

      t.timestamps
    end

    create_table :accesses_institutes, id: false do |t|
      t.belongs_to :access, index: true
      t.belongs_to :institute, index: true
    end
  end
end
