class AddLocalToAd < ActiveRecord::Migration[6.0]
  def change
    add_column :ads, :local, :integer
    add_column :ads, :image, :string
    add_column :ads, :expire_at, :datetime
    add_column :ads, :name, :string
  end
end
