class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.string :name, null: false
      t.datetime :start_at, null: false
      t.integer :status, null: false, default: 0
      t.datetime :published_at
      t.string :map_url, null: true
      t.string :link, null: true

      t.timestamps
    end
  end
end
