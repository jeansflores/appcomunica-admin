class CreateDevices < ActiveRecord::Migration[6.0]
  def change
    create_table :devices do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.string :token, null: false

      t.timestamps
    end
  end
end
