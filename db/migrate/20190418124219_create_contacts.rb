class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.references :owner, null: false, polymorphic: true
      t.string :phone, null: true, limit: 15
      t.string :email, null: true
      t.string :whatsapp_phone, null: true, limit: 15

      t.timestamps
    end
  end
end
