class AddLogoToSponsor < ActiveRecord::Migration[6.0]
  def change
    add_column :sponsors, :logo, :string
  end
end
