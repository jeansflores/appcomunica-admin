class CreateRaffles < ActiveRecord::Migration[6.0]
  def change
    create_table :raffles do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.references :company, index: true, foreign_key: true, null: false
      t.references :research, index: true, foreign_key: true, null: true
      t.string :name, null: false
      t.date :raffled_at, null: false
      t.integer :amount_available, null: false
      t.integer :status, null: false, default: 0
      t.boolean :single, null: false, default: false
      t.integer :percent_votes, null: false, default: 0
      t.datetime :published_at

      t.timestamps
    end
  end
end
