class CreateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :cities do |t|
      t.references :state, null: false, index: true, foreign_key: true, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
