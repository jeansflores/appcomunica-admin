class AddImageToAdItems < ActiveRecord::Migration[6.0]
  def change
    add_column :ad_items, :image, :string
  end
end
