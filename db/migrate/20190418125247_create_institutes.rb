class CreateInstitutes < ActiveRecord::Migration[6.0]
  def change
    create_table :institutes do |t|
      t.string :social_name, null: false
      t.string :trade_name, null: false
      t.string :cnpj, null: false, limit: 14

      t.timestamps
    end
  end
end
