class RemoveAdItem < ActiveRecord::Migration[6.0]
  def up
    drop_table :ad_items

    execute <<-SQL
      DELETE FROM ads WHERE id = 1;
    SQL
  end
end
