class DropColumnInRaffles < ActiveRecord::Migration[6.0]
  def change
    remove_column :raffles, :percent_votes
  end
end
