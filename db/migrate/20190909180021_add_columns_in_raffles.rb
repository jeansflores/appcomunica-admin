class AddColumnsInRaffles < ActiveRecord::Migration[6.0]
  def change
    add_column :raffles, :votes_needed, :integer, null: false, default: 0
  end
end
