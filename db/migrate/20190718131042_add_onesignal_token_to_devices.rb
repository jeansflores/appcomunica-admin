class AddOnesignalTokenToDevices < ActiveRecord::Migration[6.0]
  def change
    add_column :devices, :onesignal_token, :string, null: false
  end
end
