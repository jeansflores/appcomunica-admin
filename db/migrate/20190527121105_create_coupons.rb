class CreateCoupons < ActiveRecord::Migration[6.0]
  def change
    create_table :coupons do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :promotion, index: true, foreign_key: true, null: false
      t.string :token, null: true

      t.timestamps
    end

    add_index :coupons, [:user_id, :promotion_id], unique: true
    add_index :coupons, [:promotion_id, :user_id], unique: true
  end
end
