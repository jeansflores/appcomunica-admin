class CreateVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :votes do |t|
      t.references :research_participant, index: true, foreign_key: true, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.integer :rating, null: false, default: 0

      t.timestamps
    end

    add_index :votes, [:research_participant_id, :user_id], unique: true
    add_index :votes, [:user_id, :research_participant_id], unique: true
  end
end
