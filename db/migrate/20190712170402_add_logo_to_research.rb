class AddLogoToResearch < ActiveRecord::Migration[6.0]
  def change
    add_column :researches, :logo, :string
  end
end
