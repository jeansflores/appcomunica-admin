class AddInstituteColumnToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :institute, index: true, foreign_key: true

    add_index :users, [:email, :institute_id], unique: true
  end
end
