class AddLogoToInstitute < ActiveRecord::Migration[6.0]
  def change
    add_column :institutes, :logo, :string
  end
end
