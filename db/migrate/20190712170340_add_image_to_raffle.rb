class AddImageToRaffle < ActiveRecord::Migration[6.0]
  def change
    add_column :raffles, :image, :string
  end
end
