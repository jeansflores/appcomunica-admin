class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.references :source, polymorphic: true, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.integer :activity_type, null: false

      t.timestamps
    end
  end
end
