class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.references :category, index: true, foreign_key: true, null: false
      t.string :social_name, null: false
      t.string :trade_name, null: false
      t.string :cnpj, null: false, limit: 14

      t.timestamps
    end
  end
end
