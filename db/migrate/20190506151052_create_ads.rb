class CreateAds < ActiveRecord::Migration[6.0]
  def change
    create_table :ads do |t|
      t.references :institute, index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end
