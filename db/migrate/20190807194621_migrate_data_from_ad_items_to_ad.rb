class MigrateDataFromAdItemsToAd < ActiveRecord::Migration[6.0]
  def up
    Setting.create(institute_id: 1, max_ads: 20)

    execute <<-SQL
      INSERT INTO ads (institute_id, local, image, expire_at, created_at, updated_at, name)
        select
          1,
          ai.local,
          ai.image,
          localtimestamp + interval '365 day',
          ai.created_at,
          ai.updated_at,
          'anuncios'
        from ad_items as ai;
    SQL
  end
end
