class CreateAdItems < ActiveRecord::Migration[6.0]
  def change
    create_table :ad_items do |t|
      t.references :ad, index: true, foreign_key: true, null: false
      t.integer :local, null: false

      t.timestamps
    end
  end
end
