class CreateResearchParticipants < ActiveRecord::Migration[6.0]
  def change
    create_table :research_participants do |t|
      t.references :research, index: true, foreign_key: true, null: false
      t.references :category, index: true, foreign_key: true, null: false
      t.references :company, index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end
