class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :raffle, index: true, foreign_key: true, null: false
      t.string :token, null: true
      t.boolean :raffled, default: false, null: false

      t.timestamps
    end

    add_index :tickets, [:user_id, :raffle_id], unique: true
    add_index :tickets, [:raffle_id, :user_id], unique: true
  end
end
