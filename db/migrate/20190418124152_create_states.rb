class CreateStates < ActiveRecord::Migration[6.0]
  def change
    create_table :states do |t|
      t.references :country, null: false, index: true, foreign_key: true, null: false
      t.string :name, null: false
      t.string :acronym, null: false, limit: 2

      t.timestamps
    end
  end
end
