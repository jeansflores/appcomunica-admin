class DeviseCreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: true, default: ""
      t.string :facebook_id
      t.string :google_id

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at
    
      ## Custom
      t.string :name, null: false
      t.boolean :active, null: false, default: true
      t.boolean :admin, null: false, default: false
      t.integer :origin, null: false, default: 0
      t.integer :role, null: false, default: 2

      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
  end
end
