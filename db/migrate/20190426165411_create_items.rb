class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.string :name, null: false
      t.integer :item_type, null: false

      t.timestamps
    end
  end
end
