class AddBannerToResearch < ActiveRecord::Migration[6.0]
  def change
    add_column :researches, :banner, :string
  end
end
