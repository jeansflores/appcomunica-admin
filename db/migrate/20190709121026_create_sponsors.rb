class CreateSponsors < ActiveRecord::Migration[6.0]
  def change
    create_table :sponsors do |t|
      t.string  :name
      t.references :research, index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end
