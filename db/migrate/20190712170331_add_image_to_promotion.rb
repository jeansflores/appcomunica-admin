class AddImageToPromotion < ActiveRecord::Migration[6.0]
  def change
    add_column :promotions, :image, :string
  end
end
