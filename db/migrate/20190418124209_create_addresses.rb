class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.references :owner, polymorphic: true, null: false
      t.references :city, index: true, foreign_key: true, null: false
      t.string :zipcode, limit: 9, null: false
      t.string :district, null: false
      t.string :street, null: false
      t.string :number
      t.string :complement
      t.string :map_url, null: true

      t.timestamps
    end
  end
end
