class AddColumnsInPromotions < ActiveRecord::Migration[6.0]
  def change
    add_reference :promotions, :research, foreign_key: true, null: true
    add_column :promotions, :single, :boolean, null: false, default: true
    add_column :promotions, :votes_needed, :integer, null: false, default: 0
  end
end
