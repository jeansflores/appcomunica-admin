class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.references :institute, index: true, foreign_key: true, null: false
      t.integer :max_ads, default: 6, null: false
    end
  end
end
