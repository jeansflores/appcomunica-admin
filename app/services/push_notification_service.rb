class PushNotificationService
  def self.notify_all(*attrs)
    new(*attrs).notify_all
  end

  def self.notify_by_user(players_ids, *attrs)
    new(*attrs).notify_by_user(players_ids)
  end

  def initialize(title, content, options = {})
    title = title
    content = content
    send_after = options.fetch(:send_after, nil)

    @notification = build_notification(title, content, send_after)

    @onesignal_client = OneSignal::Client.new(auth_token: ENV['ONESIGNAL_AUTH_TOKEN'], app_id: ENV['ONESIGNAL_APP_ID'])
  end

  def notify_all
    notification[:included_segments] = ["All"]

    send_notification
  end

  def notify_by_user(players_ids)
    notification[:include_player_ids] = players_ids

    send_notification
  end

  private

  attr_reader :notification, :onesignal_client

  def send_notification
    onesignal_client.notifications.create(notification) if Rails.env.production?
  end

  def build_notification(title, content, send_after)
    properties = {
      android_accent_color: "143a85",
      contents: {
        en: content,
      },
      delayed_option: 'immediate',
      headings: {
        en: title,
      },
      ios_category: "alert",
      priority: 10,
      small_icon: "@drawable/ic_notification",
    }

    properties[:send_after] = send_after unless send_after.nil?

    properties
  end
end
