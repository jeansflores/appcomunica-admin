class ResearchParticipantService

  def self.build_research_participants(*attr)
    new(*attr).build_research_participants
  end

  def initialize(companies, research_participant_data, research_id)
    @companies = companies
    @research_participant_data = research_participant_data
    @research_id = research_id
  end

  def create_research_participants
    if @research_participant_data[:research][:select_all] == '1'
      ResearchParticipant.insert_all(
        build_research_participants(@research_id, @companies)
      )
    else
      selected_categories = @research_participant_data[:categories] || []

      if selected_categories.present?
        ResearchParticipant.insert_all(
          build_research_participants(
            @research_id, @companies.from_category(selected_categories)
          )
        )
      end

      if @research_participant_data[:companies].present?
        JSON.parse(@research_participant_data[:companies]).each do |category, companies|
          category_id = category.scan(/\d/).join

          ResearchParticipant.insert_all(
            [].tap do |participants|
              if selected_categories.exclude?(category_id)
                companies.each do |company|
                  participants << {
                    research_id: @research_id,
                    category_id: category_id,
                    company_id: company['company_id'],
                    created_at: DateTime.now,
                    updated_at: DateTime.now
                  }
                end
              end
            end
          ) if companies.present?
        end
      end
    end
    Research.reset_counters(@research_id, :research_participants)
  end

  def update_research_participants
    companies_used = Research.find(@research_id).companies.pluck(:id)
    institute_id = @research_participant_data[:research][:institute_id]
    companies_available = Company.from_institute(institute_id).where.not(id: companies_used)

    if @research_participant_data[:research][:select_all] == '1'
      ResearchParticipant.insert_all(
        build_research_participants(@research_id, companies_available)
      ) if companies_available.present?
    elsif @research_participant_data[:categories].blank? &&
      JSON.parse(@research_participant_data[:companies]).blank? &&
      JSON.parse(@research_participant_data[:unchanged_values]).blank?

      ResearchParticipant.from_research(@research_id).delete_all
    else
      selected_companies_by_categories = @research_participant_data[:companies] || []
      selected_categories = @research_participant_data[:categories] || []
      unchanged_values = JSON.parse(@research_participant_data[:unchanged_values])

      JSON.parse(selected_companies_by_categories).each do |key, selected_companies_by_category|

        category_id = key.gsub(/\D+/, '')

        if unchanged_values.include?(category_id)
          next
        end

        companies_on_form = selected_companies_by_category.pluck('company_id')

        companies_on_research = ResearchParticipant.
            where(research_id: @research_id, category_id: category_id).
            pluck(:company_id)

        companies_to_add = companies_on_form - companies_on_research
        companies_to_remove = companies_on_research - companies_on_form

          ResearchParticipant.
            where(company_id: companies_to_remove, research_id: @research_id).
            where.not(category_id: unchanged_values).
            delete_all

        Research.reset_counters(@research_id, :research_participants)

        ResearchParticipant.insert_all!(
          [].tap do |participants|
            companies_to_add.each do |company|
              participants << {
                research_id: @research_id,
                category_id: category_id,
                company_id: company,
                created_at: DateTime.now,
                updated_at: DateTime.now
              }
            end
          end
        ) if companies_to_add.present?
      end
    end
    Research.reset_counters(@research_id, :research_participants)
  end

  def build_research_participants(research_id, companies)
    [].tap do |items|
      companies.each do |company|
        items << {
          research_id: research_id,
          category_id: company.category_id,
          company_id: company.id,
          created_at: DateTime.now,
          updated_at: DateTime.now
        }
      end
    end
  end
end

