class RaffleService
  def self.call
    Raffle.open.raffle_current_day.includes(:tickets).each do |raffle|
      winners = []
      tickets = raffle.tickets
      availables = raffle.amount_available

      while(!availables.zero?)
        break if tickets.count.zero?

        availables -= 1

        winner = tickets.sample

        tickets = tickets.reject { |ticket| ticket.id == winner.id }

        winner.update!(raffled: true)

        winners << winner
      end

      raffle.update!(status: :closed)

      if winners.any?
        winners.each do |winner|
          if winner.user.devices.any?
            PushNotificationService.notify_by_user(
              [winner.user.devices.last.onesignal_token],
              I18n.t("activerecord.models.#{raffle.class.name.downcase}.other"),
              raffle.name
            )
          end
        end

        Backend::RaffleMailer.notify_winners(raffle, winners).deliver_now if raffle.company.contact.email.present?
      end
    end
  end

  def self.create_tickets(raffle, users = [])
    return if raffle.single?

    user_for_notification = []

    users = User.from_research(raffle.research_id) if users.blank?

    users.each do |user|
      if user.votes.from_research(raffle.research_id).count >= raffle.minimum_votes &&
        user.tickets.map(&:raffle_id).exclude?(raffle.id)
        Ticket.create(user: user, raffle: raffle)

        user_for_notification << user.devices.last.onesignal_token if user.devices.any?
      end
    end

    if user_for_notification.any?
      PushNotificationService.notify_by_user(
        user_for_notification.compact,
        "Parabéns",
        "Você está participando do sorteio #{raffle.name}"
      )
    end
  end
end
