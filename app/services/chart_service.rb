class ChartService
  def self.get_votes_by_day(days_range, votes)
    votes_by_day = votes.created_at_range(days_range).order(:created_at)

    votes_by_day.group_by do |vote|
      I18n.l(vote.created_at.to_date, format: :short)
    end.map{ |key, value| {key => value.count} }.inject(:merge)
  end

  def self.get_votes_by_category(votes)
    votes.includes(research_participant: :category).
      group_by{ |vote| vote.research_participant.category.name }.
      map{ |key, value| {key => value.count} }.inject(:merge)
  end

  def self.get_votes_by_company(votes)
    votes.includes(research_participant: :company).
      group_by{ |vote| vote.research_participant.company.id}.
      map{ |_key, vote| { vote[0].research_participant.company.trade_name => vote.count } }.
      sort_by{ |vote| -vote.values[0] }.first(20).inject(:merge)
  end

  def self.get_new_users(days_range)
    new_users_of_the_week =
      User.by_origin([:app, :site]).created_at_range(days_range).
        group_by { |user| user.created_at.to_date }.
        sort_by{ |user| user[0] }.
        map{ |key, value| { I18n.l(key, format: :short) => value } }.
        inject(:merge)

    new_users = new_users_of_the_week.
      map{ |key, value| { key  => value.count } }.inject(:merge)

    return unless new_users
    new_users_site = new_users_of_the_week.
      map do |key, values|
        { key  => values.select{ |value| value[:origin] == 'site' }.count }
      end.inject(:merge)

    new_users_app = new_users_of_the_week.
      map do |key, values|
        { key  => values.select{ |value| value[:origin] == 'app' }.count }
      end.inject(:merge)

    { all: new_users, app: new_users_app, site: new_users_site }
  end
end
