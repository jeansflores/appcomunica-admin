class PromotionService
  def self.check(promotion, coupon = nil)
    available_coupons = promotion.amount_available - promotion.coupons.count

    promotion.update!(status: 3) if available_coupons.zero?

    if coupon.present? && promotion.company.contact.email.present?
      Backend::PromotionMailer.notify(promotion, coupon, available_coupons).deliver_now
    end
  end

  def self.finalize
    Promotion.open.finalize_current_day.each do |promotion|
      promotion.update!(status: 3)
    end
  end

  def self.check_available_coupon(promotion, users = [])
    return if promotion.single?

    user_for_notification = []

    users = User.from_research(promotion.research_id) if users.blank?

    users.each do |user|
      if user.votes.from_research(promotion.research_id).count >= promotion.minimum_votes &&
        user.coupons.map(&:promotion_id).exclude?(promotion.id) && user.devices.any?

        # Backend::PromotionMailer.notify_winner(promotion, user, coupon).deliver_now if user.email.present?

        user_for_notification << user.devices.last.onesignal_token
      end
    end

    if user_for_notification.any?
      PushNotificationService.notify_by_user(
        user_for_notification.compact,
        "Parabéns",
        "Você está apto a resgatar a promoção #{promotion.name}"
      )
    end
  end
end
