class PublicationService
  def self.publish(object, status = :published)
    object.transaction do
      object.update!(status: status, published_at: Time.current)

      case object.class.name
      when 'Post'
        PushNotificationService.notify_all(
          I18n.t("activerecord.models.#{object.class.name.downcase}.other"),
          object.title
        )
      when 'Notification'
        PushNotificationService.notify_all(
          object.title,
          object.message,
          send_after: object.schedule
        )
      when 'Raffle', 'Promotion', 'Event'
        PushNotificationService.notify_all(
          I18n.t("activerecord.models.#{object.class.name.downcase}.other"),
          object.name
        )
      end
    rescue Exception => e
    end
  end

  def self.cancel(object)
    object.update!(status: :canceled, published_at: nil)
  end
end
