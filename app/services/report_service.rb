class ReportService
  def self.generate_report(params, type, data_type = 'data')
    "#{type}Report".constantize.send("report_#{data_type}", params)
  end
end
