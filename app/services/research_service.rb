class ResearchService
  def self.finalize
    Research.open.finalize_current_day.each do |research|
      research.update!(status: 3)
    end
  end
end
