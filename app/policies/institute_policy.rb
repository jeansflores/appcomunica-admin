class InstitutePolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    @user.manager? && @user.admin?
  end

  def new?
    create?
  end

  def update?
    ["opened", "closed", "canceled"].exclude?(record.status) || (record.opened? && !record.coupons.any?)
  end

  def edit?
    update?
  end

  def destroy?
    ["opened", "closed", "canceled"].exclude?(record.status) || !record.coupons.any?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
