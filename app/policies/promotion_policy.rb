class PromotionPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    ["opened", "closed", "canceled"].exclude?(record.status) || (record.opened? && record.coupons.blank?)
  end

  def edit?
    update?
  end

  def destroy?
    ["opened", "closed", "canceled"].exclude?(record.status) || (record.opened? && record.coupons.blank?)
  end

  def publish?
    record.awaiting?
  end

  def cancel_publication?
    record.awaiting?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
