module DefaultMailer
  extend ActiveSupport::Concern

  private

  def dispatch(record, type, options = {})
    return true unless Rails.env.production?

    initialize_from_record(record)
    mail headers_for(type, options)
  end

end
