class Devise::Mailer < Devise.parent_mailer.constantize
  include Devise::Mailers::Helpers
  include DefaultMailer

  default from: ENV['DEFAULT_EMAIL_SENDER']
  layout 'mailer'

  def confirmation_instructions(record, token, opts={})
    @token = token
    @institute = record.institute
    dispatch(record, :confirmation_instructions, opts)
  end

  def reset_password_instructions(record, token, opts={})
    @token = token
    @institute = record.institute
    dispatch(record, :reset_password_instructions, opts)
  end

  def unlock_instructions(record, token, opts={})
    @token = token
    @institute = record.institute
    dispatch(record, :unlock_instructions, opts)
  end

  def email_changed(record, opts={})
    @institute = record.institute
    dispatch(record, :email_changed, opts)
  end

  def password_change(record, opts={})
    @institute = record.institute
    dispatch(record, :password_change, opts)
  end
end
