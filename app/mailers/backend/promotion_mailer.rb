class Backend::PromotionMailer < ApplicationMailer
  default template_path: "mailers/#{self.name.underscore}"

  def notify(promotion, coupon, available_coupons)
    @institute = promotion.institute
    @promotion = promotion
    @coupon = coupon
    @available_coupons = available_coupons

    mail(to: @promotion.company.contact.email, subject: "Cupom solicitado da promoção: #{@promotion.name}")
  end

  def notify_winner(promotion, user, coupon)
    @promotion = promotion
    @user = user
    @coupon = coupon

    mail(to: @user.email, subject: "Parabéns, você está participando da promoção: #{@promotion.name}")
  end
end
