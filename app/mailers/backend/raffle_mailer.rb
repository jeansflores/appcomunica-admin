class Backend::RaffleMailer < ApplicationMailer
  default template_path: "mailers/#{self.name.underscore}"

  def notify_winners(raffle, winners)
    @institute = raffle.institute
    @raffle = raffle
    @winners = winners

    mail(to: @raffle.company.contact.email, subject: "Ganhadores do sorteio: #{@raffle.name}")
  end
end
