class ApplicationMailer < ActionMailer::Base
  include DefaultMailer
  default from: ENV['DEFAULT_EMAIL_SENDER']
  layout 'mailer'
end
