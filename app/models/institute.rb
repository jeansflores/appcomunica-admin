class Institute < ApplicationRecord
  extend Pagy::Search

  has_one :address, as: :owner, dependent: :destroy
  has_one :contact, as: :owner, dependent: :destroy
  has_one :open_research, -> { open }, class_name: 'Research'
  has_one :setting, dependent: :destroy

  has_many :ads, dependent: :destroy
  has_many :companies, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :users, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :promotions, dependent: :destroy
  has_many :coupons, through: :promotions
  has_many :raffles, dependent: :destroy
  has_many :researches, dependent: :destroy
  has_many :research_participants, through: :researches
  has_many :votes, through: :researches
  has_many :tickets, through: :raffles
  has_many :regular_users, -> { active.regular }, class_name: "User"

  has_and_belongs_to_many :accesses

  has_rich_text :about

  mount_uploader :logo, ImageUploader

  accepts_nested_attributes_for :address, :setting, :contact, allow_destroy: true

  delegate :phone, to: :contact, allow_nil: true
  delegate :city, :country, :state, :zipcode, :district, :street, :number, :complement,
    to: :address, allow_nil: true

  validates :social_name, :about, :cnpj, :trade_name, :logo, presence: true
  validates :cnpj, length: { maximum: 14 }, uniqueness: true

  def to_s
    trade_name
  end

  def self.search(search_params)
    if search_params.present?
      order(:social_name)
        .where(
          "social_name ILIKE :value OR
          trade_name ILIKE :value OR
          cnpj ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(:social_name)
    end
  end

  def address_id
    address.id
  end

  def contact_id
    contact.id
  end
end
