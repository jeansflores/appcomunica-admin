class Company < ApplicationRecord
  active_record_base_class = ActiveRecord::Base

  extend Pagy::Search

  attr_reader :research

  multi_tenant :institute

  belongs_to :category
  belongs_to :institute

  has_one :address, as: :owner, dependent: :destroy
  has_one :contact, as: :owner, dependent: :destroy
  has_one :research_participant,  dependent: :restrict_with_error

  has_many :promotion, dependent: :destroy
  has_many :raffle, dependent: :destroy
  has_many :research_participant, dependent: :destroy

  has_rich_text :about

  mount_uploader :logo, ImageUploader

  accepts_nested_attributes_for :address, :contact, allow_destroy: true

  delegate :phone, to: :contact, allow_nil: true
  delegate :city, :country, :state, :zipcode, :district, :street, :number, :complement,
    to: :address, allow_nil: true

  validates :social_name, :about, :trade_name, :cnpj, :institute_id, :category_id, presence: true
  validates :cnpj, length: { maximum: 14 }, uniqueness: true

  scope :from_institute, -> institute_id { where(institute_id: institute_id) }
  scope :from_category, -> category_id { where(category_id: category_id) }

  scope :on_research, -> research_id do
    joins(research_participant: :research).where(researches: { id: research_id })
  end

  scope :from_institute_and_category, -> institute_id, category_id do
    where(institute_id: institute_id, category_id: category_id)
  end

  def to_s
    trade_name
  end

  def full_address
    address.full
  end

  def logo_url
    logo? ? logo&.medium.url : nil
  end

  scope :with_research_participant, -> research_id, category_id do
    active_record_base_class.connection.execute(
      "SELECT
        companies.id,
        companies.trade_name,
        companies.cnpj,
        companies.category_id,
        research_participants.research_id,
        contacts.email
      FROM companies
      LEFT JOIN research_participants
        ON (research_participants.company_id = companies.id)
        AND research_participants.research_id = #{research_id}
      LEFT JOIN contacts ON contacts.owner_id = companies.id
        AND contacts.owner_type = 'Company'
      WHERE companies.category_id = #{category_id}
      ORDER BY 5"
    ).to_a
  end

  scope :on_research, -> research_id do
    joins(:research_participant).where(research_participants: { research_id: research_id })
  end

  scope :not_on_research, -> research_id, category_id do
    where.not(id: ResearchParticipant.from_category(category_id).from_research(research_id).select(:company_id))
  end

  def self.search(search_params)
    if search_params.present?
      order(:social_name)
        .joins(:category, :institute)
        .where(
          "companies.social_name ILIKE :value OR
          companies.trade_name ILIKE :value OR
          companies.cnpj ILIKE :value OR
          institutes.trade_name ILIKE :value OR
          categories.name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(:social_name)
    end
  end

  def address_id
    address.id
  end

  def contact_id
    contact.id
  end
end
