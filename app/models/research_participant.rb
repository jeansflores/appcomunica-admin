class ResearchParticipant < ApplicationRecord
  belongs_to :research, counter_cache: :total_participants
  belongs_to :category
  belongs_to :company

  has_many :votes, dependent: :destroy

  validates :category_id, :company_id, presence: true

  scope :from_research, -> research_id { where(research_id: research_id) }
  scope :from_category, -> category_id { where(category_id: category_id) }
  scope :from_company, -> company_id { where(company_id: company_id) }
  scope :not_from_company, -> company_id { where.not(company_id: company_id) }
end
