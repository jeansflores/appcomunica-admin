class Activity < ApplicationRecord
  belongs_to :user
  belongs_to :source, polymorphic: true

  enum activity_type: [ :a, :b ]

  translate_enum :activity_type

  validates :user_id, :source_id, :source_type, :activity_type, presence: true

  scope :stargazers, -> { where(activity_type: 'a') }
  scope :sharers, -> { where(activity_type: 'b') }
end
