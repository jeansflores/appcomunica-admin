class Setting < ApplicationRecord
  belongs_to :institute

  validates :max_ads, numericality: { only_integer: true, greater_than_or_equal_to: 0 },
    presence: true

  validate :can_create_ads?

  private

  def can_create_ads?
    if institute.ads.active.count > max_ads
      errors.add(:base, I18n.t('errors.messages.max_ads_should_be_greater_than_active_ads'))

      return false
    end

    true
  end
end
