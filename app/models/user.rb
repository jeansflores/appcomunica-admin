class User < ApplicationRecord
  extend Pagy::Search

  devise :database_authenticatable, :registerable, :recoverable, :rememberable

  multi_tenant :institute

  belongs_to :institute, optional: true

  has_many :tickets, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :coupons, dependent: :destroy
  has_many :research_participant, through: :votes
  has_many :researches, through: :research_participants
  has_many :devices

  mount_uploader :avatar, ImageUploader

  enum origin: [:app, :site, :dashboard]
  enum role: [:manager, :institute, :regular]

  translate_enum :role

  validates :name, :email, :role, :institute_id, presence: true
  validates :email, uniqueness: { scope: :institute_id } if :email_changed?
  validates :email, email: true
  validates :password, length: { in: 8..16 }, presence: true, on: :create, unless: :check_social_id?
  validates :password, length: { in: 8..16 }, allow_blank: true, on: :update, unless: :check_social_id?

  scope :from_research, -> research_id do
    joins(:institute, votes: [research_participant: :research]).
      where(votes: { research_participants: { researches: { id: research_id } } }).distinct
  end

  scope :active, -> { where(active: true) }
  scope :by_origin, -> values { where(origin: values) }
  scope :dashboard, -> { where(role: [ :manager, :institute ]) }
  scope :regular, -> { where(role: :regular) }

  scope :created_at_range, -> range do
    where(created_at:(DateTime.current.beginning_of_day)-range.day..DateTime.current.end_of_day)
  end

  def active_for_authentication?
    super && active?
  end

  def full_manager?
    manager? && admin?
  end

  def self.search(search_params)
    if search_params.present?
      order(:name)
        .joins(:institute)
        .where(
          "users.name ILIKE :value OR
          users.email ILIKE :value OR
          institutes.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      dashboard.order(:name)
    end
  end

  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.current

    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 1.hours) > Time.current
  end

  def reset_password(password)
    self.reset_password_token = nil
    self.password = password

    save
  end

  def full_manager?
    manager? && admin?
  end

  private

  def generate_token
    SecureRandom.hex(3)
  end

  def check_social_id?
    facebook_id || google_id
  end
end
