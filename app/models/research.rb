class Research < ApplicationRecord
  extend Pagy::Search

  attr_reader :select_all

  multi_tenant :institute

  belongs_to :institute

  has_many :promotions, dependent: :destroy
  has_many :raffles, dependent: :destroy
  has_many :research_participants, dependent: :delete_all
  has_many :categories, through: :research_participants
  has_many :companies, through: :research_participants
  has_many :votes, through: :research_participants
  has_many :sponsors, dependent: :destroy

  has_rich_text :regulation
  has_rich_text :description

  mount_uploader :logo, ImageUploader
  mount_uploader :banner, ImageUploader

  enum status: [ :awaiting, :canceled, :opened, :closed ]

  translate_enum :status

  accepts_nested_attributes_for :research_participants, allow_destroy: true

  validates :institute_id, :name, :start_at, :end_at, :status, :regulation, :description,
    :logo, :banner, presence: true

  validate :can_create_research?

  scope :open, -> { where(status: 'opened') }
  scope :open_awaiting, -> { where(status: ['opened', 'awaiting']) }
  scope :finalize_current_day, -> { where(end_at: Date.current) }
  scope :from_institute, -> institute_id { where(institute_id: institute_id) }

  scope :not_from_company, -> company_id do
    where.not(id: from_company(company_id))
  end

  scope :from_company, -> company_id do
    joins(:research_participants).
      where(research_participants: { company_id: company_id }).distinct
  end

  def company_ids
    research_participants.map(&:company_id)
  end

  def self.search(search_params)
    if search_params.present?
      order(id: :desc)
        .joins(:institute)
        .where(
          "researches.name ILIKE :value OR
          institutes.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(id: :desc)
    end
  end

  def total_participants
    research_participants.size
  end

  private

  def can_create_research?
    return true unless Research.open.where.not(id: id).any?

    errors.add(:base, I18n.t('errors.messages.there_is_already_an_open_research'))

    false
  end
end
