class Address < ApplicationRecord
  belongs_to :city
  belongs_to :owner, polymorphic: true

  validates :city_id, :district, :street, :state, :country, presence: true
  validates :number, length: { maximum: 10 }
  validates :zipcode, length: { is: 8 }, allow_blank: true

  delegate :country, :state, to: :city, allow_nil: true
  delegate :name, to: :city, prefix: true, allow_nil: true
  delegate :acronym, to: :state, prefix: true, allow_nil: true

  def full
    str = street
    str << ", #{number}" if number.present?
    str << ", #{complement}" if complement.present?
    str << ", #{district}" if district.present?
    str << ", #{I18n.t('activerecord.attributes.address.zipcode')}: #{zipcode}" if zipcode.present?
    str << ", #{state}" if state.present?

    str
  end
end
