class Ability
  include CanCan::Ability

  def initialize(current_user)
    return can :manage, :all if current_user.manager? && current_user.admin? && current_user.institute.nil?

    can :manage, [User, Institute, Item] if current_user.manager? && !current_user.admin?
    can :manage, Notification

    if (current_user.manager? || current_user.institute?) &&
      !current_user.admin? && current_user.institute.present?
      current_user.institute.accesses.each do |access|
        can :manage, access.key.singularize.capitalize.constantize

        if access.key.in?(["researches","promotions","raffles"])
          can :manage, Category
          can :manage, Company
        end
      end

      can :me, User
      can :update_me, User
    end
  end
end
