class Ad < ApplicationRecord
  extend Pagy::Search

  multi_tenant :institute

  belongs_to :institute

  validates :institute_id, :name, :image, :expire_at, :local, presence: true

  validate :can_create_ads_by_institute?

  mount_uploader :image, ImageUploader

  enum local: [ :events, :news, :promotions, :raffles ]

  translate_enum :local

  scope :active, -> { where("expire_at > ?", DateTime.now) }

  def self.search(search_params)
    if search_params.present?
      order(id: :desc)
        .joins(:institute)
        .where(
          "institutes.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(id: :desc)
    end
  end

  private

  def can_create_ads_by_institute?
    if institute.ads.active.count >= institute.setting.max_ads && was_expired?
      errors.add(:base, I18n.t('errors.messages.ads_limit_reached'))

      return false
    end

    true
  end

  def was_expired?
    if id
      return Ad.find(id).expire_at < DateTime.now.beginning_of_day
    end

    true
  end
end
