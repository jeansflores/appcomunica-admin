class Sponsor < ApplicationRecord
  belongs_to :research

  mount_uploader :logo, ImageUploader

  validates :name, :logo, presence: true

  validate :check_max_amount_by_research

  private

  def check_max_amount_by_research
    max_amount = 10

    if research.sponsors.size >= max_amount
      errors.add(
        :base,
        I18n.t(
          'messages.errors.max_amount_by_research',
          max_amount: max_amount,
        )
      )

      return false
    end

    true
  end
end
