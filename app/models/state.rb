class State < ApplicationRecord
  belongs_to :country

  has_many :cities, dependent: :restrict_with_exception

  validates :acronym, :name, :country_id, presence: true
  validates :acronym, length: {is: 2}

  scope :from_country, -> country_id {where(country_id: country_id)}

  def to_s
    "#{name} - #{acronym}"
  end
end
