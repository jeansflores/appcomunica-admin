class Category < ApplicationRecord
  extend Pagy::Search

  has_many :research_participants
  has_many :companies

  validates :name, presence: true, uniqueness: true

  scope :from_institute, -> institute_id do
    joins(:companies).where(companies: { institute_id: institute_id }).distinct.order(:name)
  end

  scope :on_research, -> research_id do
    joins(:research_participants).
      where(research_participants: { research_id: research_id }).distinct
  end

  def self.search(search_params)
    if search_params.present?
      order(:name)
        .where(
          "name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(:name)
    end
  end

end
