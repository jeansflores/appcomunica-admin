class Vote < ApplicationRecord
  active_record_base_class = ActiveRecord::Base

  after_create do
    research.raffles.open.each do |raffle|
      RaffleService.create_tickets(raffle, [user])
    end

    research.promotions.open.each do |promotion|
      PromotionService.check_available_coupon(promotion, [user])
    end
  end

  belongs_to :research_participant
  belongs_to :user

  has_one :research, through: :research_participant
  has_one :category, through: :research_participant

  validates :rating, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }, presence: true

  validate :can_create, :duplicated_vote, on: :create

  scope :from_research, -> research_id do
    joins(:research_participant).where(research_participants: { research_id: research_id })
  end

  scope :created_at_range, -> range do
    where(created_at:(DateTime.current.beginning_of_day)-range.day..DateTime.current.end_of_day)
  end

  scope :votes_by_category_from_research, -> research_id, started_at, end_at do
    active_record_base_class.connection.execute(
      "SELECT
        c.name,
        r.id AS research_id,
        count(v.id) AS votes
      FROM researches AS r
      INNER JOIN research_participants AS rp ON rp.research_id = r.id
      INNER JOIN categories AS c ON c.id = rp.category_id
      INNER JOIN votes AS v ON v.research_participant_id = rp.id
      WHERE research_id = #{research_id} AND
      v.created_at BETWEEN '#{started_at}' AND '#{end_at}'
      GROUP BY 1, 2
      ORDER BY votes DESC"
    )
  end

  scope :company_votes_by_category, -> research_id, started_at, end_at, by_category = false do
    active_record_base_class.connection.execute(
      "WITH table_data AS (
        SELECT
          c.id,
          c.trade_name as trade_name,
          ad.street as address,
          cat.name AS category_name,
          c.cnpj,
          COUNT(CASE WHEN v.rating = 5 THEN v.rating END) AS five_stars,
          COUNT(CASE WHEN v.rating = 4 THEN v.rating END) AS four_stars,
          COUNT(CASE WHEN v.rating = 3 THEN v.rating END) AS three_stars,
          COUNT(CASE WHEN v.rating = 2 THEN v.rating END) AS two_stars,
          COUNT(CASE WHEN v.rating = 1 THEN v.rating END) AS one_star,
          COUNT(v.id) AS total_ratings
        FROM companies AS c
        INNER JOIN research_participants AS rp ON rp.company_id = c.id
        INNER JOIN votes AS v ON v.research_participant_id = rp.id
        INNER JOIN categories AS cat ON cat.id = rp.category_id
        INNER JOIN addresses AS ad ON ad.owner_id = c.id AND ad.owner_type = 'Company'
        WHERE rp.research_id = #{research_id} AND
        v.created_at BETWEEN '#{started_at}' AND '#{end_at}'
        GROUP BY 1,2,3,4
      ), report_value AS (
        SELECT t.*, (t.five_stars*10 + t.four_stars*5 + t.three_stars*2 + t.one_star*(-1)) AS points
        FROM table_data as t
        ORDER BY #{by_category ? "4 ASC," : ""} 12 DESC, 6 DESC, 7 DESC, 8 DESC, 9 DESC, 10 DESC
      ) SELECT * FROM report_value"
    )
  end

  def can_create
    return true if research.opened?

    errors.add(:base, I18n.t('errors.messages.research_is_no_longer_available'))

    false
  end

  def duplicated_vote
    if Vote.select do |vote|
        vote.user_id == user_id && vote.research_participant_id == research_participant_id
      end.any?
      errors.add(:base, I18n.t('errors.messages.you_can_not_vote_again_in_the_same_company'))

      return false
    end

    true
  end
end
