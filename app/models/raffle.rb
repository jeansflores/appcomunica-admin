class Raffle < ApplicationRecord
  extend Pagy::Search

  multi_tenant :institute

  belongs_to :institute
  belongs_to :company
  belongs_to :research, optional: true

  has_many :activities, as: :source, dependent: :destroy
  has_many :tickets, dependent: :destroy

  has_rich_text :description
  has_rich_text :regulation

  mount_uploader :image, ImageUploader

  enum status: [ :awaiting, :canceled, :opened, :closed ]

  translate_enum :status

  validates :company_id, :name, :raffled_at, :amount_available, :status, :description, :regulation,
    :image, presence: true

  validates :amount_available, numericality: { only_integer: true, greater_than: 0 }
  validates :votes_needed, numericality: { only_integer: true, greater_than: 0 }, if: -> { !single }
  validates :research_id, :votes_needed, presence: true, if: -> { !single }

  validate :can_create_raffle_single?

  scope :open, -> { where(status: 'opened') }
  scope :raffle_current_day, -> { where(raffled_at: Date.current) }

  def stargazers_count
    activities.stargazers.count
  end

  def sharers_count
    activities.sharers.count
  end

  def minimum_votes
    single? ? 0 : votes_needed
  end

  def self.search(search_params)
    if search_params.present?
      order(id: :desc)
        .joins(:company)
        .where(
          "raffles.name ILIKE :value OR
          companies.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(id: :desc)
    end
  end

  private

  def can_create_raffle_single?
    if !single? && research.nil?
      errors.add(:base, I18n.t('errors.messages.you_must_link_a_research'))

      return false
    end

    true
  end
end
