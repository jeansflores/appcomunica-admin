class Item < ApplicationRecord
  belongs_to :institute

  has_rich_text :description

  mount_uploader :image, ImageUploader

  enum item_type: [ :product, :service ]

  translate_enum :item_type

  validates :name, :description, :item_type, :image, presence: true

  scope :from_institute, -> institute_id { where(institute_id: institute_id) }
end
