class Ticket < ApplicationRecord
  before_create do
    self.token = SecureRandom.hex(4)
  end

  belongs_to :user
  belongs_to :raffle

  validate :can_create, :duplicated_ticket, on: :create

  scope :raffled, -> { where(raffled: true) }

  def can_create
    return true if raffle.opened?

    errors.add(:base, I18n.t('errors.messages.raffle_is_no_longer_available'))

    false
  end

  def duplicated_ticket
    if Ticket.select { |ticket| ticket.user_id == user_id && ticket.raffle_id == raffle_id }.any?
      errors.add(:base, I18n.t('errors.messages.it_is_not_possible_to_take_more_than_one_ticket_by_raffle'))

      return false
    end

    true
  end
end
