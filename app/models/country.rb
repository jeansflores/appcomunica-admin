class Country < ApplicationRecord
  has_many :states, dependent: :restrict_with_exception

  validates :name, :code, presence: true
  validates :code, length: {is: 2}, numericality: true
end
