class Post < ApplicationRecord
  extend Pagy::Search

  multi_tenant :institute

  belongs_to :institute

  has_many :activities, as: :source, dependent: :destroy

  has_rich_text :content

  mount_uploader :image, ImageUploader

  enum status: [ :awaiting, :canceled, :published ]

  translate_enum :status

  validates :institute_id, :title, :content, :image, presence: true

  def stargazers_count
    activities.stargazers.count
  end

  def sharers_count
    activities.sharers.count
  end

  def self.search(search_params)
    if search_params.present?
      order(id: :desc)
        .joins(:institute)
        .where(
          "posts.title ILIKE :value OR
          institutes.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(id: :desc)
    end
  end
end
