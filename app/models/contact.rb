class Contact < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :email, presence: true
  validates :phone, length: { minimum: 10, maximum: 11 }, presence: true

  validates :whatsapp_phone, length: { minimum: 10, maximum: 11 },
    presence: true, :if => Proc.new{ owner_type == "Institute" }

  scope :by_owner, -> owner_id { where(owner_id: owner_id) }
end
