class Notification < ApplicationRecord
  extend Pagy::Search

  attr_accessor :hour

  belongs_to :institute

  enum status: [ :awaiting, :canceled, :published ]

  translate_enum :status

  validate :valid_schedule?

  validates :title, :institute_id, :message, presence: true
  validates :schedule, presence: true, if: -> { scheduled }

  private

  def valid_schedule?
    if scheduled
      if !(schedule? && schedule >= DateTime.now)
        errors.add(:base, I18n.t('errors.messages.invalid_date'))
        return false
      end
    end

    true
  end
end
