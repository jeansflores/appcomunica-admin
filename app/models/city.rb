class City < ApplicationRecord
  belongs_to :state

  has_many :addresses, dependent: :restrict_with_exception

  delegate :country, to: :state, allow_nil: true

  validates :state_id, :name, presence: true

  scope :from_state, -> state_id { where(state_id: state_id) }

  def full
    "#{name}/#{state.acronym}"
  end
end
