class Event < ApplicationRecord
  extend Pagy::Search

  attr_accessor :hour

  multi_tenant :institute

  belongs_to :institute

  has_many :activities, as: :source, dependent: :destroy

  has_rich_text :description

  mount_uploader :image, ImageUploader

  enum status: [ :awaiting, :canceled, :opened, :closed ]

  translate_enum :status

  validates :institute_id, :name, :start_at, :status, :description, :image, presence: true

  scope :open, -> { where(status: 'opened') }

  def self.search(search_params)
    if search_params.present?
      order(id: :desc)
        .joins(:institute)
        .where(
          "events.name ILIKE :value OR
          institutes.trade_name ILIKE :value",
          value: "%#{search_params[:value]}%"
        )
    else
      all.order(id: :desc)
    end
  end
end
