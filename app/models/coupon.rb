class Coupon < ApplicationRecord
  before_create do
    self.token = SecureRandom.hex(5)
  end

  after_create do
    PromotionService.check(promotion, self)
  end

  belongs_to :user
  belongs_to :promotion

  validate :can_create, :duplicated_coupon, on: :create

  def can_create
    if promotion.coupons.size < promotion.amount_available &&
      promotion.opened? && Date.current <= promotion.expires_at
      return true
    end

    errors.add(:base, I18n.t('errors.messages.promotion_is_no_longer_available'))

    false
  end

  def duplicated_coupon
    if Coupon.find_by(user_id: user_id, promotion_id: promotion_id).present?
      errors.add(:base, I18n.t('errors.messages.it_is_not_possible_to_take_more_than_one_coupon_by_promotion'))

      return false
    end

    true
  end
end
