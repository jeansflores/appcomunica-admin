module TranslateHelper
  def explain(model_name = model_file)
    I18n.t ("activerecord.models.#{model_name}.explain")
  end

  def plural(klass = resource_class)
    klass.model_name.human(count: 'many')
  end

  def singular(klass = resource_class)
    klass.model_name.human
  end
end
