module HomeHelper
  def link_more_info(opts = {})
    link_to(
      raw(I18n.t('home.more_info') + ' <i class="fa fa-arrow-circle-right"></i>'),
      send(opts[:path]),
      class: "small-box-footer"
    )
  end

  def link_render_modal(type)
    link_to(
      raw("<i class='fa fa-arrows-alt'></i>"),
      open_modal_path(type_chart: type),
      data: { remote: true, toggle: :modal, target: 'modal-window' }
    )
  end
end
