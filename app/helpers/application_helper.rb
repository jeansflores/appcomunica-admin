module ApplicationHelper
  include Pagy::Frontend

  def menu(model, icon)
    render 'layouts/application/menu', { model: model, controller: model.downcase.pluralize, icon: icon }
  end

  def flash_messages
    flash.to_h.slice('alert', 'notice')
  end

  def flash_icons(type)
    case type
    when 'alert' then 'ban'
    when 'notice' then 'check'
    end
  end

  def flash_context_map(type)
    case type
    when 'alert' then 'danger'
    when 'notice' then 'success'
    end
  end

  def link_show(object, opts = {})
    if policy(object).show?
      link_to(
        raw("<i class='fa fa-eye icon-action'></i>"),
        send(opts[:path], object), data: { toggle: :tooltip, placement: :top },
        title: I18n.t('button.view')
      )
    end
  end

  def link_edit(object, opts = {})
    if policy(object).edit?
      link_to(
        raw("<i class='fa fa-pencil-square-o icon-action'></i>"),
        send(opts[:path], object), data: { toggle: :tooltip, placement: :top },
        title: I18n.t('button.edit')
      )
    end
  end

  def link_destroy(object, opts = {})
    if policy(object).destroy?
      link_to(
        raw("<i class='fa fa-trash icon-action red'></i>"),
        send(opts[:path], object), method: :delete,
        data: { confirm: I18n.t('messages.confirm.sure'), toggle: :tooltip, placement: :top },
        title: I18n.t('button.destroy')
      )
    end
  end

  def link_publish(object, opts = {})
    if policy(object).publish?
      link_to(
        raw("<i class='fa fa-arrow-circle-up icon-action green'></i>"),
        send(opts[:path], object), method: :post,
        data: { confirm: I18n.t('messages.confirm.sure'), toggle: :tooltip, placement: :top },
        title: I18n.t('button.publish')
      )
    end
  end

  def link_cancel_publication(object, opts = {})
    if policy(object).cancel_publication?
      link_to(
        raw("<i class='fa fa-times-circle icon-action red'></i>"),
        send(opts[:path], object), method: :post,
        data: { confirm: I18n.t('messages.confirm.sure'), toggle: :tooltip, placement: :top },
        title: I18n.t('button.cancel_publish')
      )
    end
  end

  def button_to_generate_file(report, format)
    icon = format == :xlsx ? 'excel' : format

    button_to(
      icon.capitalize,
      report_researches_generate_file_path(format: format),
      params: report,
      class: 'btn',
      form: { style: 'display:inline-block;' }
    )
  end

  def submenu(controller, menu, level = 1, params = {})
    render 'layouts/application/submenu',
      { controller: controller, menu: menu, level: level, params: params }
  end

  def tag_status(object, opts = {})
    case object.status
    when 'awaiting' then raw("<span class='label label-warning'>#{object.translated_status}</span>")
    when 'canceled' then raw("<span class='label label-danger'>#{object.translated_status}</span>")
    when 'closed' then raw("<span class='label label-danger'>#{object.translated_status}</span>")
    when 'opened' then raw("<span class='label label-success'>#{object.translated_status}</span>")
    when 'published'
      if object.class.name == 'Notification' && object.schedule? && DateTime.now < object.schedule
        raw("<span class='label bg-purple'>#{I18n.t('general.scheduled')}</span>")
      else
        raw("<span class='label label-primary'>#{object.translated_status}</span>")
      end
    end
  end
end
