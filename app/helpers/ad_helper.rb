module AdHelper
  def can_create_ad?(&block)
    if @current_user.full_manager? || @current_user.institute.ads.count < @current_user.institute.setting.max_ads
      content_tag(:div, capture(&block))
    end
  end

  def ad_status(object)
    object.expire_at > DateTime.now.beginning_of_day ?
      raw("<span class='label label-primary'>#{I18n.t('activerecord.attributes.ad.active')}</span>") :
      raw("<span class='label label-danger'>#{I18n.t('activerecord.attributes.ad.expired')}</span>")
  end
end
