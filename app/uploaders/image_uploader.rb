class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :fog

  def move_to_cache
    true
  end

  def move_to_store
    true
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w(jpg jpeg png)
  end

  def content_type_whitelist
    /image\//
  end

  version :large, if: :is_large?
  version :medium, if: :is_medium?
  version :slim, if: :is_slim?

  version :large do
    process resize_to_fit: [600, 350]
  end

  version :medium do
    process resize_to_fit: [200, 200]
  end

  version :slim do
    process resize_to_fit: [600, 150]
  end

  private

  def is_large? picture
    [:image, :banner].include?(mounted_as) && model.class.name != "Ad"
  end

  def is_medium? picture
    [:avatar, :logo].include?(mounted_as)
  end

  def is_slim? picture
    model.class.name == "Ad"
  end
end
