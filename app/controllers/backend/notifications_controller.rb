class Backend::NotificationsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Notification.all.order(created_at: :desc)
    @pagy, @notifications = pagy(records, size: [2,2,2,2] )
  end

  def new
    @notification = authorize Notification.new
  end

  def create
    @notification = authorize Notification.new(notification_params)

    if @notification.save
      return respond_with @notification, location: notification_path(@notification)
    end

    respond_with @notification
  end

  def edit
    @notification = authorize Notification.find(params[:id])
  end

  def update
    @notification = authorize Notification.find(params[:id])

    if @notification.update(notification_params)
      return respond_with @notification, location: notification_path(@notification)
    end

    respond_with @notification
  end

  def destroy
    @notification = authorize Notification.find(params[:id])

    @notification.destroy

    respond_with @notification
  end

  def publish
    @notification = authorize Notification.find(params[:id])

    PublicationService.publish(@notification)

    respond_with @notification, location: notifications_path()
  end

  def cancel_publication
    @notification = authorize Notification.find(params[:id])

    PublicationService.cancel(@notification)

    respond_with @notification, location: notifications_path()
  end

  def notification_params
    if params[:notification][:schedule].present? && params[:notification][:hour].present?
      params[:notification][:schedule] = "#{params[:notification][:schedule]} #{params[:notification][:hour]}"
    end

    params.require(:notification).
      permit(
        :title,
        :message,
        :institute_id,
        :scheduled,
        :hour,
        :schedule,
      )
  end
end
