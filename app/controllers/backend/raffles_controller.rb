class Backend::RafflesController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Raffle.search(params[:search])
    @pagy, @raffles = pagy(records, size: [2,2,2,2] )
  end

  def show
    @raffle = authorize Raffle.find(params[:id])
  end

  def new
    @institute = @institutes.find(params[:raffle][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @researches = Research.from_institute(@institute.id).open
    @raffle = authorize Raffle.new
  end

  def create
    @institute = @institutes.find(params[:raffle][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @researches = Research.from_institute(@institute.id).open
    @raffle = authorize Raffle.new(raffle_params)

    if @raffle.save
      return respond_with @raffle, location: raffle_path(@raffle)
    end

    respond_with @raffle
  end

  def edit
    @raffle = authorize Raffle.find(params[:id])
    @raffle.image.cache!
    @companies = Company.from_institute(@raffle.institute.id)
    @researches = Research.from_institute(@raffle.institute.id).open
  end

  def update
    @raffle = authorize Raffle.find(params[:id])
    @companies = Company.from_institute(@raffle.institute.id)
    @researches = Research.from_institute(@raffle.institute.id).open

    if @raffle.update(raffle_params)
      return respond_with @raffle, location: raffle_path(@raffle)
    end

    respond_with @raffle
  end

  def destroy
    @raffle = authorize Raffle.find(params[:id])

    @raffle.destroy

    respond_with @raffle
  end

  def publish
    @raffle = authorize Raffle.find(params[:id])

    if PublicationService.publish(@raffle, :opened)
      RaffleService.create_tickets(@raffle)
    end

    respond_with @raffle, location: raffles_path()
  end

  def cancel_publication
    @raffle = authorize Raffle.find(params[:id])

    PublicationService.cancel(@raffle)

    respond_with @raffle, location: raffles_path()
  end

  private

  def raffle_params
    params.require(:raffle).
      permit(
        :name,
        :raffled_at,
        :amount_available,
        :status,
        :single,
        :votes_needed,
        :description,
        :regulation,
        :image,
        :image_cache,
        :company_id,
        :institute_id,
        :research_id
      )
  end
end
