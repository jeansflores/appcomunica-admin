class Backend::PromotionsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Promotion.search(params[:search])
    @pagy, @promotions = pagy(records, size: [2,2,2,2] )
  end

  def show
    @promotion = authorize Promotion.find(params[:id])
  end

  def new
    @institute = @institutes.find(params[:promotion][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @researches = Research.from_institute(@institute.id).open
    @promotion = authorize Promotion.new
  end

  def create
    @institute = @institutes.find(params[:promotion][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @researches = Research.from_institute(@institute.id).open
    @promotion = authorize Promotion.new(promotion_params)

    if @promotion.save
      return respond_with @promotion, location: promotion_path(@promotion)
    end

    respond_with @promotion
  end

  def edit
    @promotion = authorize Promotion.find(params[:id])
    @promotion.image.cache!
    @companies = Company.from_institute(@promotion.institute.id)
    @researches = Research.from_institute(@promotion.institute.id).open
  end

  def update
    @promotion = authorize Promotion.find(params[:id])
    @companies = Company.from_institute(@promotion.institute.id)
    @researches = Research.from_institute(@promotion.institute.id).open

    if @promotion.update(promotion_params)
      return respond_with @promotion, location: promotion_path(@promotion)
    end

    respond_with @promotion
  end

  def destroy
    @promotion = authorize Promotion.find(params[:id])

    @promotion.destroy

    respond_with @promotion
  end

  def publish
    @promotion = authorize Promotion.find(params[:id])

    if PublicationService.publish(@promotion, :opened)
      PromotionService.check_available_coupon(@promotion)
    end

    respond_with @promotion, location: promotions_path()
  end

  def cancel_publication
    @promotion = authorize Promotion.find(params[:id])

    PublicationService.cancel(@promotion)

    respond_with @promotion, location: promotions_path()
  end

  private

  def promotion_params
    params.require(:promotion).
      permit(
        :name,
        :expires_at,
        :amount_available,
        :status,
        :single,
        :votes_needed,
        :description,
        :image,
        :image_cache,
        :company_id,
        :institute_id,
        :research_id
      )
  end
end
