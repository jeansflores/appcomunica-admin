class Backend::UsersController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = User.search(params[:search])
    @pagy, @users = pagy(records, size: [2,2,2,2])
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      return respond_with @user, location: user_path(@user)
    end

    respond_with @user
  end

  def edit
    @user = User.find(params[:id])
    @user.avatar.cache!
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      return respond_with @user, location: user_path(@user)
    end

    respond_with @user
  end

  def me
    @user = @current_user
  end

  def update_me
    @user = User.find(params[:user][:id])

    if @user.update(user_params)
      return respond_with @user, location: root_path
    end

    render :me
  end

  private

  def user_params
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    params[:user][:origin] = 2

    params.require(:user).
      permit(
        :institute_id,
        :name,
        :email,
        :password,
        :avatar,
        :avatar_cache,
        :role,
        :active,
        :origin
      )
  end
end
