class Backend::CategoriesController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Category.search(params[:search])
    @pagy, @categories = pagy(records, size: [2,2,2,2] )
  end

  def show
    @category = Category.find(params[:id])
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      return respond_with @category, location: categories_path()
    end

    respond_with @category
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])

    if @category.update(category_params)
      return respond_with @category, location: categories_path()
    end

    respond_with @category
  end

  def destroy
    @category = Category.find(params[:id])

    unless @category.destroy
      flash[:alert] = @category.errors.messages[:base].join(',') unless @category.errors.messages[:base].blank?
    end

    respond_with @category, location: categories_path()
  end

  def from_research
    collection = Research.find(params[:research_id]).categories.distinct

    respond_to do |format|
      format.json { render json: collection }
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
