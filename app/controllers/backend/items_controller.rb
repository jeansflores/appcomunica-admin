class Backend::ItemsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html
  before_action :load_dependencies, only: [:new, :create, :edit, :update, :show, :destroy]

  def show
    @item = Item.find(params[:id])
  end

  def new
    @item = Item.new(institute_id: @institute.id)
  end

  def create
    @item = Item.new(item_params)

    if @item.save
      return respond_with @item, location: institute_item_path(@institute, @item)
    end

    respond_with @item
  end

  def edit
    @item = Item.find(params[:id])
    @item.image.cache!
  end

  def update
    @item = Item.find(params[:id])

    if @item.update(item_params)
      return respond_with @item, location: institute_item_path(@institute, @item)
    end

    respond_with @item
  end

  def destroy
    @item = Item.find(params[:id])

    @item.destroy

    respond_with @item, location: institute_path(@institute)
  end

  private

  def load_dependencies
    @institute = @institutes.find(params[:institute_id])
  end

  def item_params
    params.require(:item).
      permit(
        :description,
        :image,
        :image_cache,
        :institute_id,
        :item_type,
        :name
      )
  end
end
