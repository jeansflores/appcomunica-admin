class Backend::ReportResearchesController < Backend::Controller
  respond_to :html, :xlsx, :pdf
  load_and_authorize_resource :research, :parent => false
  before_action :check_permissions

  def index
    @researches = Research.all
    @categories = Category.all
    @ranking_type = ['general', 'company_by_category', 'category']
  end

  def report
    if params[:research_report][:research].blank? || params[:research_report][:ranking].blank?
      return redirect_to(
        report_researches_path,
        alert: I18n.t('report.research.error.required_fields_missing')
      )
    end

    @report = ReportService.generate_report(params[:research_report], 'Research')
    @pagy, @report_data = pagy_array(JSON[@report[:data]], size: [2,2,2,2])
  end

  def generate_file
    if params[:format] == 'pdf'
      @logo = Research.find(params[:research][:id]).institute.logo

      respond_to do |format|
        format.pdf do
          render pdf: I18n.t('report.research.ranking.' << params[:type]),
          footer: { right: "[page] #{I18n.t('report.research.general_labels.of')} [topage]" },
          orientation: 'Landscape',
          template: "backend/report_researches/layout_pdf.html.erb"
        end
      end
    elsif params[:format] == 'xlsx'
      report = ReportService.generate_report(params, 'Research', 'excel')

      respond_to do |format|
        format.xlsx { render xlsx: report[:file], filename: I18n.t('report.research.file_name.' << params[:type]) }
      end
    end
  end

  private

  def check_permissions
    raise CanCan::AccessDenied unless @current_user.full_manager?
  end
end
