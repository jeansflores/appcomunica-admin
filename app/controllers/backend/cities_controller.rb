class Backend::CitiesController < Backend::Controller
  has_scope :from_state

  def index
    @collection = apply_scopes(City).all

    respond_to do |format|
      format.js
    end
  end
end
