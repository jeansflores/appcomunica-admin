class Backend::StatesController < Backend::Controller
  has_scope :from_country

  def index
    @collection = apply_scopes(State).all

    respond_to do |format|
      format.js
    end
  end
end
