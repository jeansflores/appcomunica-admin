class Backend::CompaniesController < Backend::Controller
  load_and_authorize_resource
  respond_to :html, :json
  before_action :load_dependencies, only: [:new, :edit, :create, :update]

  def index
    records = Company.search(params[:search])
    @pagy, @companies = pagy(records, size: [2,2,2,2] )
  end

  def show
    @company = Company.find(params[:id])
  end

  def new
    @company = Company.new
  end

  def create
    if params[:commit] == I18n.t("true") && params[:company][:research].present?
      @company = Company.new(company_params)

      if @company.save
        ResearchParticipant.create!(
          research_id: params[:company][:research],
          company_id: @company.id,
          category_id: @company.category.id,
        )

        return respond_with @company, location: company_path(@company)
      end

      respond_with @company
    else
      @company = Company.new(company_params)

      if @company.save
        return respond_with @company, location: company_path(@company)
      end

      respond_with @company
    end
  end

  def link_company_to_research
    if params[:connect_to_research][:research].present?
      company_id = params[:connect_to_research][:company]
      company = Company.find(company_id)

      ResearchParticipant.create!(
        research_id: params[:connect_to_research][:research],
        company_id: company_id,
        category_id: company.category_id
      )
    end

    redirect_to companies_path
  end

  def edit
    @company = Company.find(params[:id])
    @company.logo.cache!
  end

  def update
    @company = Company.find(params[:id])

    if @company.update(company_params)
      return respond_with @company, location: company_path(@company)
    end

    respond_with @company
  end

  def destroy
    @company = Company.find(params[:id])

    unless @company.destroy
      flash[:alert] = @company.errors.messages[:base].join(',') unless @company.errors.messages[:base].blank?
    end

    respond_with @company, location: companies_path()
  end

  def from_research
    scope = Company.on_research(params[:research_id]).includes(:contact, :address)
    scope = scope.from_category(params[:category_id]) if params[:category_id].present?
    @companies = scope

    respond_to do |format|
      format.json { render 'backend/companies/company.json' }
    end
  end

  def from_category_and_institute
    @companies = Company.from_institute(params[:institute_id]).from_category(params[:category_id])

    respond_to do |format|
      format.json { render :company }
    end
  end

  private

  def load_dependencies
    @categories = Category.all
    @countries = Country.all
    @cities = @states = []

    unless params[:id].blank?
      company = Company.find(params[:id])

      @states = State.from_country(company.country.id) if company.country
      @cities = City.from_state(company.state.id) if company.state
    end

    if params.key?(:company) && company_params[:address_attributes][:city_id].present?
      city = City.find(company_params[:address_attributes][:city_id])

      @states = State.from_country(city.country.id) if city.country
      @cities = City.from_state(city.state.id) if city.state
    end
  end

  def company_params
    params.require(:company).
      permit(
        :about,
        :trade_name,
        :social_name,
        :cnpj,
        :email,
        :logo,
        :logo_cache,
        :category_id,
        :institute_id,
        address_attributes: [
          :city_id,
          :zipcode,
          :district,
          :street,
          :number,
          :complement,
          :map_url
        ],
        contact_attributes: [
          :phone,
          :email,
          :whatsapp_phone
        ]
      )
  end
end
