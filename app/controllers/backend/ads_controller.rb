class Backend::AdsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Ad.search(params[:search])
    @pagy, @ads = pagy(records, size: [2,2,2,2] )
  end

  def show
    @ad = Ad.find(params[:id])
  end

  def new
    @ad = Ad.new
  end

  def create
    @ad = Ad.new(ad_params)

    if @ad.save
      return respond_with @ad, location: ads_path(@ad)
    end

    flash[:alert] = @ad.errors.messages[:base].join(',') unless @ad.errors.messages[:base].blank?

    respond_with @ad
  end

  def edit
    @ad = Ad.find(params[:id])
    @ad.image.cache!
  end

  def update
    @ad = Ad.find(params[:id])

    if @ad.update(ad_params)
      return respond_with @ad, location: ads_path(@ad)
    end

    flash[:alert] = @ad.errors.messages[:base].join(',') unless @ad.errors.messages[:base].blank?

    respond_with @ad
  end

  def destroy
    @ad = Ad.find(params[:id])

    @ad.destroy

    respond_with @ad
  end

  private

  def ad_params
    params.require(:ad).
      permit(
        :expire_at,
        :institute_id,
        :image,
        :image_cache,
        :local,
        :name
      )
  end
end
