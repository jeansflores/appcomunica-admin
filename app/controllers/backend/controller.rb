class Backend::Controller < ApplicationController
  include Pundit
  include Pagy::Backend

  set_current_tenant_through_filter

  before_action :authenticate_user!
  before_action :set_customer_as_tenant

  helper_method :resource_name, :resource_class, :resource, :can?

  rescue_from Exception, with: -> { render_error 500 }
  rescue_from CanCan::AccessDenied, with: -> { render_error 403 }
  rescue_from Pundit::NotAuthorizedError, with: -> { render_error 403 }
  rescue_from ActiveRecord::RecordNotFound, with: -> { render_error 404 }
  rescue_from ActionController::RoutingError, with: -> { render_error 404 }

  protected

  def resource_name
    controller_name.singularize
  end

  def resource_class
    resource_name.capitalize.constantize
  end

  def resource
    if instance_variable_defined?(:"@#{resource_name}")
      instance_variable_get(:"@#{resource_name}")
    else
      instance_variable_set("@#{resource_name}", resource_class.find_by(id: params[:id]))
    end
  end

  def resource_params
    if respond_to?("#{resource_name}_#{action_name}_params")
      send("#{resource_name}_#{action_name}_params")
    elsif respond_to?("#{resource_name}_params")
      send("#{resource_name}_params")
    else
      params.fetch(resource_name, {})
    end
  end

  def resource=(new_resource)
    instance_variable_set(:"@#{helpers.resource_name}", new_resource)
  end

  def set_customer_as_tenant
    if current_user.manager? && current_user.admin? && current_user.institute.nil?
      @institutes = Institute.all
    else
      @institutes = Institute.where(id: current_user.institute_id)

      customer = current_user.institute
      set_current_tenant(customer)
    end
  end

  def render_error(status = 404)
    return raise unless Rails.env.production?

    respond_to do |format|
      format.html { render file: "#{Rails.root}/public/#{status}.html", status: status }
      format.all { render nothing: true, status: status }
    end
  end

  private

  def class_exists?(class_name)
    klass = Module.const_get(class_name.to_s)

    klass.is_a?(Class)
  rescue NameError
    false
  end
end
