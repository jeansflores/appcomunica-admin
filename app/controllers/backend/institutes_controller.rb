class Backend::InstitutesController < Backend::Controller
  load_and_authorize_resource
  respond_to :html
  before_action :load_dependencies, only: [:new, :edit, :create, :update]

  def index
    records = @institutes.search(params[:search])
    @pagy, @collection = pagy(records, size: [2,2,2,2] )
  end

  def show
    @institute = @institutes.find(params[:id])
  end

  def new
    @institute = Institute.new
  end

  def create
    @institute = Institute.new(institute_params)

    if @institute.save
      return respond_with @institute, location: institute_path(@institute)
    end

    respond_with @institute
  end

  def edit
    @institute = @institutes.find(params[:id])
    @institute.logo.cache!
  end

  def update
    @institute = @institutes.find(params[:id])

    if @institute.update(institute_params)
      return respond_with @institute, location: institute_path(@institute)
    end

    if @institute.errors.messages[:'setting.base'].any?
      flash[:alert] = @institute.errors.messages[:'setting.base'].join(', ')
    end

    respond_with @institute
  end

  def destroy
    @institute = @institutes.find(params[:id])

    @institute.destroy

    respond_with @institute
  end

  private

  def load_dependencies
    @countries = Country.all
    @cities = @states = []

    unless params[:id].blank?
      institute = @institutes.find(params[:id])

      @states = State.from_country(institute.country.id) if institute.country
      @cities = City.from_state(institute.state.id) if institute.state
    end
  end

  def institute_params
    params.require(:institute).
      permit(
        :social_name,
        :trade_name,
        :cnpj,
        :state_registration,
        :about,
        :logo,
        :logo_cache,
        setting_attributes: [
          :max_ads,
        ],
        access_ids: [],
        address_attributes: [
          :city_id,
          :zipcode,
          :district,
          :street,
          :number,
          :complement,
          :map_url
        ],
        contact_attributes: [
          :phone,
          :email,
          :whatsapp_phone
        ]
      )
  end
end
