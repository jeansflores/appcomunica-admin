class Backend::HomeController < Backend::Controller
  respond_to :html, :js
  before_action :load_info_dashboard, only: [:index]

  def open_modal
    respond_to do |format|
      format.js {render partial: 'open_modal', locals: { type_chart: params[:type_chart] } }
    end
  end

  private

  def load_info_dashboard
    @count_users = 0
    @count_companies = 0
    @count_raffles = 0
    @count_tickets = 0
    @count_researches = 0
    @count_events = 0
    @count_promotions = 0
    @count_coupons = 0

    @institutes.each do |institute|
      @count_users += institute.users.by_origin([:app, :site]).count
      @count_companies += institute.companies.count
      @count_raffles += institute.raffles.open.count
      @count_tickets += institute.tickets.count
      @count_researches += institute.researches.open.count
      @count_events += institute.events.open.count
      @count_promotions += institute.promotions.open.count
      @count_coupons += institute.coupons.count
    end

    @user_current_institute = @current_user.institute

    if @user_current_institute && @user_current_institute.open_research
      @total_votes_by_day = ChartService.get_votes_by_day(6, @user_current_institute.open_research.votes)
      @votes_by_category =  ChartService.get_votes_by_category(@user_current_institute.open_research.votes)
      @votes_by_company = ChartService.get_votes_by_company(@user_current_institute.open_research.votes)
      @new_users_of_the_week = ChartService.get_new_users(6)
    end
  end
end
