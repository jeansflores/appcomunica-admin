class Backend::SponsorsController < Backend::Controller
  before_action :load_dependencies, only: [:new, :create, :edit, :update, :show, :destroy]

  def new
    @sponsor = Sponsor.new(research_id: @research.id)
  end

  def create
    @sponsor = Sponsor.new(sponsor_params)

    if @sponsor.save
      return respond_with @sponsor, location: research_sponsor_path(@research, @sponsor)
    end

    respond_with @sponsor
  end

  def edit
    @sponsor = Sponsor.find(params[:id])
  end

  def show
    @sponsor = Sponsor.find(params[:id])
  end

  def update
    @sponsor = Sponsor.find(params[:id])

    if @sponsor.update(sponsor_params)
      return respond_with @sponsor, location: research_sponsor_path(@research, @sponsor)
    end

    respond_with @sponsor
  end

  def destroy
    @sponsor = Sponsor.find(params[:id])

    @sponsor.destroy

    respond_with @sponsor, location: research_path(@research)
  end

  private

  def load_dependencies
    @research = Research.find(params[:research_id])
  end

  def sponsor_params
    params.require(:sponsor).
      permit(
        :name,
        :logo,
        :logo_cache,
        :research_id
      )
  end
end
