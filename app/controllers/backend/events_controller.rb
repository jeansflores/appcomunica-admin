class Backend::EventsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Event.search(params[:search])
    @pagy, @events = pagy(records, size: [2,2,2,2] )
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      return respond_with @event, location: event_path(@event)
    end

    respond_with @event
  end

  def edit
    @event = Event.find(params[:id])
    @event.image.cache!
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      return respond_with @event, location: event_path(@event)
    end

    respond_with @event
  end

  def destroy
    @event = Event.find(params[:id])

    @event.destroy

    respond_with @event
  end

  def publish
    @event = Event.find(params[:id])

    PublicationService.publish(@event, :opened)

    respond_with @event, location: events_path()
  end

  def cancel_publication
    @event = Event.find(params[:id])

    PublicationService.cancel(@event)

    respond_with @event, location: events_path()
  end

  private

  def event_params
    if params[:event][:start_at].present? && params[:event][:hour].present?
      params[:event][:start_at] = "#{params[:event][:start_at]} #{params[:event][:hour]}"
    end

    params.require(:event).
      permit(
        :institute_id,
        :name,
        :map_url,
        :link,
        :start_at,
        :description,
        :image,
        :image_cache
      )
  end
end
