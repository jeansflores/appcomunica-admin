class Backend::PostsController < Backend::Controller
  load_and_authorize_resource
  respond_to :html

  def index
    records = Post.search(params[:search])
    @pagy, @posts = pagy(records, size: [2,2,2,2] )
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)

    if @post.save
      return respond_with @post, location: post_path(@post)
    end

    respond_with @post
  end

  def edit
    @post = Post.find(params[:id])
    @post.image.cache!
  end

  def update
    @post = Post.find(params[:id])

    if @post.update(post_params)
      return respond_with @post, location: post_path(@post)
    end

    respond_with @post
  end

  def destroy
    @post = Post.find(params[:id])

    @post.destroy

    respond_with @post
  end

  def publish
    @post = Post.find(params[:id])

    PublicationService.publish(@post)

    respond_with @post, location: posts_path()
  end

  def cancel_publication
    @post = Post.find(params[:id])

    PublicationService.cancel(@post)

    respond_with @post, location: posts_path()
  end

  private

  def post_params
    params.require(:post).
      permit(
        :title,
        :subtitle,
        :content,
        :image,
        :image_cache,
        :institute_id
      )
  end
end
