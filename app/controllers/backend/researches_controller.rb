class Backend::ResearchesController < Backend::Controller
  load_and_authorize_resource
  respond_to :html, :js, :json

  def index
    records = Research.search(params[:search])
    @pagy, @researches = pagy(records, size: [2, 2, 2, 2])
  end

  def show
    @research = authorize Research.find(params[:id])
    @categories = @research.categories.distinct.order(:name)
    @total_companies_by_category =
      Category.
        joins(:companies).
        where(companies: { institute_id: @research.institute_id }).
        group(:id).
        pluck('categories.id, COUNT(companies.id)').
        to_h

    @selected_companies_by_category =
      Category.
        joins(:research_participants).
        where(research_participants:{ research_id: @research.id }).
        group(:id).
        pluck('categories.id, COUNT(research_participants)').
        to_h
  end

  def new
    @institute = @institutes.find(params[:research][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @research = authorize Research.new
    @categories = Category.from_institute(@institute.id)
    @total_companies_by_category =
      Category.
        joins(:companies).
        where(companies: { institute_id: @institute.id }).
        group(:id).
        pluck('categories.id, COUNT(companies.id)').
        to_h

    @selected_companies_by_category = 0
  end

  def create(options = {})
    @institute = @institutes.find(params[:research][:institute_id])
    @companies = Company.from_institute(@institute.id)
    @research = authorize Research.new(research_params)
    @categories = Category.from_institute(@institute.id)

    @total_companies_by_category =
      Category.
        joins(:companies).
        where(companies: { institute_id: @institute.id }).
        group(:id).
        pluck('categories.id, COUNT(companies.id)').
        to_h

    @selected_companies_by_category = 0

    if @research.save
      research_participant_service = ResearchParticipantService.new(
        @companies,
        params,
        @research.id
      )
      research_participant_service.create_research_participants

      return respond_with @research, location: research_path(@research)
    else
      flash[:alert] =
        @research.errors.messages[:base].join(',') unless @research.errors.messages[:base].blank?
    end

    @data_table_values = {
      select_all: params[:research][:select_all],
      categories: params[:categories] || [],
      companies: params[:companies] || {}
    }.to_json

    respond_with @research
  end

  def edit
    @research = authorize Research.find(params[:id])
    @research.logo.cache!
    @research.banner.cache!
    @institute = Institute.find(@research.institute_id)
    @categories = Category.from_institute(@institute.id)
    @is_all_selected = (@research.company_ids.size == @institute.companies.size)

    @total_companies_by_category =
      Category.
        joins(:companies).
        where(companies: { institute_id: @institute.id }).
        group(:id).
        pluck('categories.id, COUNT(companies.id)').
        to_h

    @selected_companies_by_category =
      Category.
        joins(:research_participants).
        where(research_participants:{ research_id: @research.id }).
        group(:id).
        pluck('categories.id, COUNT(research_participants)').
        to_h

    @total_companies = Company.from_institute(@institute.id).group(:category_id).size

    @companies_by_category_count =
      ResearchParticipant.from_research(@research.id).group(:category_id).size
  end

  def update
    @research = authorize Research.find(params[:id])
    @companies = Company.from_institute(@research.institute.id)

    if @research.update(research_params)
      research_participant_service = ResearchParticipantService.new(
        @companies,
        params,
        @research.id
      )

      research_participant_service.update_research_participants
      return respond_with @research, location: research_path(@research)
    end

    @data_table_values = {
      select_all: params[:research][:select_all],
      categories: params[:categories] || [],
      companies: params[:companies] || {}
    }.to_json

    respond_with @research
  end

  def destroy
    @research = authorize Research.find(params[:id])

    @research.destroy

    respond_with @research
  end

  def publish
    @research = authorize Research.find(params[:id])

    PublicationService.publish(@research, :opened)

    respond_with @research, location: researches_path()
  end

  def cancel_publication
    @research = authorize Research.find(params[:id])

    PublicationService.cancel(@research)

    respond_with @research, location: researches_path()
  end

  def active_awating
    researches = if params[:institute_id].present?
      Research.from_institute(params[:institute_id])
    elsif params[:company_id]
      Research.not_from_company(params[:company_id])
    end.open_awaiting

    respond_to do |format|
      format.json { render json: researches }
    end
  end

  def get_research_json
    research = Research.find(params[:id])

    research_json = {
      name: research[:name],
      start_at: I18n.l(research[:start_at], format: :default),
      end_at: I18n.l(research[:end_at], format: :default)
    }

    respond_to do |format|
      format.json { render json: research_json }
    end
  end

  def load_research_participants
    @companies = if params[:type].present?
      Company.
        includes(:contact).
        from_category(params[:category_id]).
        on_research(params[:research_id])
    elsif params[:research_id].present?
      Company.
        with_research_participant(
          params[:research_id],
          params[:category_id]
        )
    elsif params[:institute_id].present?
      Company.
        includes(:contact).
        from_institute(params[:institute_id]).
        from_category(params[:category_id])
    end

    respond_to do |format|
      format.json { render 'backend/research_participants/research_participants' }
    end
  end

  private

  def research_params
    build_params = params

    build_params.require(:research).
      permit(
        :name,
        :institute_id,
        :start_at,
        :end_at,
        :logo,
        :logo_cache,
        :banner,
        :banner_cache,
        :regulation,
        :description,
        research_participants_attributes: [
          :id,
          :category_id,
          :company_id,
          :_destroy
        ]
      )
  end
end
