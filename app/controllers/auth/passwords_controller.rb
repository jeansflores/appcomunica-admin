class Auth::PasswordsController < Devise::PasswordsController
  def create
    if (params[:user][:email])
      resource = User.find_by(email: params[:user][:email])

      if resource.present? && !resource.dashboard?
        flash[:alert] = I18n.t("devise.passwords.not_found")

        return redirect_to new_user_password_path
      end
    end

    super
  end
end
