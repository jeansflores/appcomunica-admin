class Auth::SessionsController < Devise::SessionsController
  def create
    if (sign_in_params[:email])
      resource = User.find_by(email: sign_in_params[:email])

      if resource.present? && !resource.dashboard?
        flash[:alert] = I18n.t("devise.failure.not_found_in_database", authentication_keys: 'E-mail')

        return redirect_to new_user_session_path
      end
    end

    super
  end
end
