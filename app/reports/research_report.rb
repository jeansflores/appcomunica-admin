class ResearchReport
  def self.report_data(research_params)
    research = Research.find(research_params[:research])
    position_count = 1

    if research_params[:start_at].blank?
      research_params[:start_at] = research.start_at
    else
      research_params[:start_at] = Date.parse(research_params[:start_at])
    end

    if research_params[:end_at].blank?
      research_params[:end_at] = research.end_at
    else
      research_params[:end_at] = Date.parse(research_params[:end_at])
    end

    case research_params[:ranking]
    when 'category'
      data =
        Vote.votes_by_category_from_research(
          research_params[:research],
          research_params[:start_at],
          research_params[:end_at]
        ).pluck('name', 'votes')

      data.each.with_index do |data_row, index|
        data_row.insert(
          0, "#{index + 1} #{I18n.t('report.research.general_labels.position').upcase}"
        )
      end

      titles = %w(general category vote.quantity).map do |title|
        I18n.t('report.research.titles.' << title).upcase
      end
    when 'general'
      data = Vote.company_votes_by_category(
        research_params[:research],
        research_params[:start_at],
        research_params[:end_at]
      ).pluck(
        'trade_name',
        'address',
        'category_name',
        'cnpj',
        'five_stars',
        'four_stars',
        'three_stars',
        'two_stars',
        'one_star',
        'total_ratings',
        'points'
      )

      last_category_position = {}

      # We want to know the number and average of votes of the previous company in the same category
      # This build a hash with the following structure:
      # category_name:{category_position, vote_quantity, vote_average}
      # This is used to know the position, number and average of votes of the last company
      # in the same category and compare with the actual company
      data.each_with_index do |data_row, index|
        if last_category_position.key?(data_row[2])
          last_category_position[data_row[2]] += 1
        else
          last_category_position.merge!({
            data_row[2] => 1
          })
        end

        data_row.insert(
          0, "#{last_category_position[data_row[2]]}"+
          "#{I18n.t('report.research.general_labels.position').upcase}"
        )

        data_row.insert(
          0, "#{index + 1} #{I18n.t('report.research.general_labels.position').upcase}"
        )
      end

      if research_params[:category].present?
        category = Category.find(research_params[:category])
        data.select!{ |data_row| data_row[4] == category[:name] }
      end

      if research_params[:company].present?
        company = Company.find(research_params[:company])

        data.select! do |data_row|
          data_row[2] == company.trade_name
          data_row[3] == company.address.street
        end
      end

      titles = %w(general category_ranking company address category cnpj five_stars four_stars
        three_stars two_stars one_star total_ratings punctuation).map do |title|
          I18n.t('report.research.titles.' << title).upcase
        end
    when 'company_by_category'
      data = Vote.company_votes_by_category(
        research_params[:research],
        research_params[:start_at],
        research_params[:end_at],
        true
      ).pluck(
        'category_name',
        'trade_name',
        'address',
        'cnpj',
        'five_stars',
        'four_stars',
        'three_stars',
        'two_stars',
        'one_star',
        'total_ratings',
        'points'
      )

      # We have a list of companies ordered by company, votes and average of votes
      # We check if the category is diferent of the previous one, if yes
      # this one is the first company of its category
      # Then we need to know if the votes or average of votes is lower than the previous one
      # If yes the company is one position under the last previous one
      data.each_with_index do |data_row, index|
        data_row.insert(
          1, "#{index + 1} #{I18n.t('report.research.general_labels.position').upcase}"
        )
      end

      if research_params[:category].present?
        category = Category.find(research_params[:category])
        data.select!{ |data_row| data_row[0] == category[:name] }
      end

      if research_params[:company].present?
        company = Company.find(research_params[:company])
        data.select! do |data_row|
          data_row[2] == company.trade_name
          data_row[3] == company.address.street
        end
      end

      titles = %w(category category_ranking company address cnpj five_stars four_stars three_stars
        two_stars one_star total_ratings punctuation).map do |title|
        I18n.t('report.research.titles.' << title).upcase
      end
    end

    {
      data: data.to_json,
      titles: titles,
      research: { id: research[:id], name: research[:name] },
      type: research_params[:ranking],
      period: {
        start: I18n.l(research_params[:start_at], format: :default),
        end: I18n.l(research_params[:end_at], format: :default)
      },
      filters: { category_name: category&.name, company_name: company&.trade_name }
    }
  end

  def self.report_excel(values)
    data = JSON[values[:data]]
    research = values[:research][:name]
    options = {
      header_style: { background_color: "91BBFF", color: "000000", align: :center, font_size: 12,
        bold: true },
      row_style: { background_color: nil, color: "000000", align: :center, font_size: 12 },
      range_styles: [
        { range: { rows: 2, columns: :all }, styles: { bold: true } },
      ],
      headers: [
        research.upcase,
        '',
        "#{I18n.t('report.research.titles.date.start')}: #{values["period"]["start"]}",
        "#{I18n.t('report.research.titles.date.end')}: #{values["period"]["end"]}"
      ],
      sheet_name: I18n.t('report.research.file_name.' << values[:type]).upcase,
      merges: [
        { range: "A1:B1" }
      ],
    }

    case values[:type]
    when 'category'
      titles = %w(general category vote.quantity).map do |title|
        I18n.t('report.research.titles.' << title).upcase
      end
    when 'general'
      titles = %w(general category_ranking company address category cnpj  five_stars four_stars
        three_stars two_stars one_star total_ratings punctuation).map do |title|
          I18n.t('report.research.titles.' << title).upcase
        end

      options.merge!({
        headers: [
          research.upcase,
          '',
          '',
          "#{I18n.t('report.research.titles.date.start')}: #{values["period"]["start"]}",
          "#{I18n.t('report.research.titles.date.end')}: #{values["period"]["end"]}"
        ],
        merges: [
          { range: "A1:C1" }
        ]
      })

    when 'company_by_category'
      titles = %w(category category_ranking company address cnpj five_stars four_stars three_stars
        two_stars one_star total_ratings punctuation).map do |title|
        I18n.t('report.research.titles.' << title).upcase
      end
    end

    data.insert(0, titles)

    options.merge!(data: data)

    { file: SpreadsheetArchitect.to_xlsx(options), type: values["type"] }
  end
end
