class JsonWebToken
  def self.encode(payload, exp = 7.days.from_now.to_i )
    payload[:exp] = exp
    JWT.encode(payload, ENV['SECRET_KEY_BASE'])
  end

  def self.decode(token)
    return HashWithIndifferentAccess.new(JWT.decode(token, ENV['SECRET_KEY_BASE'])[0])
  rescue
    nil
  end

  def self.valid_payload?(payload)
    Time.at(payload['exp'].to_i) < Time.now
  end
end
