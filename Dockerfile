FROM ruby:alpine

RUN apk add build-base tzdata postgresql-dev yarn git less imagemagick \
    \
    && adduser -D appcomunica

ENV GEM_HOME /home/appcomunica/.bundle
ENV BUNDLE_PATH="$GEM_HOME" \
    BUNDLE_APP_CONFIG="$GEM_HOME"
ENV PATH $GEM_HOME/bin:$BUNDLE_PATH/gems/bin:$PATH

USER appcomunica

RUN gem install bundler
