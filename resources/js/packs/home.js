import Chart from "chart.js/dist/Chart"

$("#users_by_day").ready(function() {
  var context = $("#users_by_day")
    .get(0)
    .getContext("2d")

  var data = {
    labels: Object.keys(newUsersOfTheWeek.all),
    datasets: [
      {
        label: newUsersLegend,
        data: Object.values(newUsersOfTheWeek.all),
        fill: false,
        pointHoverBackgroundColor: "#DD4B39",
        lineTension: 0.1,
        backgroundColor: "#DD4B39",
        borderColor: "#DD4B39",
        pointBackgroundColor: "#DD4B39"
      },
      {
        label: newUsersFromAppLegend,
        data: Object.values(newUsersOfTheWeek.app),
        fill: false,
        pointHoverBackgroundColor: "#00C0EF",
        lineTension: 0.1,
        backgroundColor: "#00C0EF",
        borderColor: "#00C0EF",
        pointBackgroundColor: "#00C0EF"
      },
      {
        label: newUsersFromSiteLegend,
        data: Object.values(newUsersOfTheWeek.site),
        fill: false,
        pointHoverBackgroundColor: "#00A65A",
        lineTension: 0.1,
        backgroundColor: "#00A65A",
        borderColor: "#00A65A",
        pointBackgroundColor: "#00A65A"
      }
    ]
  }

  var options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  }

  var usersByDayChart = new Chart(context, {
    type: "line",
    data,
    options
  })
})

$("#vote_by_day_chart").ready(function() {
  var context = $("#vote_by_day_chart")
    .get(0)
    .getContext("2d")

  var data = {
    labels: Object.keys(votesByDay),
    datasets: [
      {
        label: votesLegend,
        data: Object.values(votesByDay),
        backgroundColor: "rgba(0, 192, 239, 0.5)",
        borderWidth: 1
      }
    ]
  }

  var options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  }

  var votesByDayChartChart = new Chart(context, {
    type: "bar",
    data,
    options
  })
})

$("#vote_by_category_chart").ready(function() {
  var context = $("#vote_by_category_chart")
    .get(0)
    .getContext("2d")

  var options = {
    legend: {
      display: false
    }
  }

  var data = {
    labels: Object.keys(votesByCategory),
    datasets: [
      {
        data: Object.values(votesByCategory),
        backgroundColor: Object.keys(votesByCategory).map(() =>
          getRandomColor()
        )
      }
    ]
  }

  var voteByCategoryChart = new Chart(context, {
    type: "pie",
    data,
    options
  })
})

$("#vote_by_company_chart").ready(function() {
  var context = $("#vote_by_company_chart")
    .get(0)
    .getContext("2d")

  var options = {
    legend: {
      display: false
    }
  }

  var data = {
    labels: Object.keys(votesByCompany),
    datasets: [
      {
        data: Object.values(votesByCompany),
        backgroundColor: Object.keys(votesByCompany).map(() => getRandomColor())
      }
    ]
  }

  var voteByCompanyChart = new Chart(context, {
    type: "pie",
    data,
    options
  })
})

function getRandomColor() {
  var letters = "0123456789ABCDEF"
  var color = "#"
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }

  return color
}
