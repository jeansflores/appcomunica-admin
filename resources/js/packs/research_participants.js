import 'datatables.net-select-bs'
import dataTablePortuguese from '../../i18n/datatables/pt-BR'

$(".category").one('click', function() {
  var categoryId = $(this).prop('id')

  if(action == 'show') {
    var researchUrl = `${url}?research_id=${researchId}&category_id=${categoryId}&type=show`
  } else if (researchId) {
    var researchUrl = `${url}?research_id=${researchId}&category_id=${categoryId}`
  } else {
    var researchUrl = `${url}?institute_id=${instituteId}&category_id=${categoryId}`
  }

  $('.table-responsive.' + categoryId).html(tableTemplate(categoryId))
  var table = $('#category' + categoryId).DataTable({
    processing: true,
    bLengthChange: false,
    deferRender: true,
    ordering: false,
    ajax: {
      url: researchUrl,
      type: 'GET'
    },
    stateSave: true,
    columns: [
      {data: ''},
      {data: 'trade_name'},
      {data: 'cnpj'},
      {data: 'email'}
    ],
    columnDefs: [{
      targets: 0,
      defaultContent: '',
      orderable: false,
      className: action == 'show' ? '' : 'select-checkbox'
    }],
    initComplete: function(settings) {
      if(action !== 'show') {
        var table = $('#category' + categoryId).DataTable()
        if($(`#check_all_companies_from_category${categoryId}`).prop('checked')) {
          table.rows().select()
        } else if(!$.isEmptyObject(tableData.companies)) {
          fillTable(tableData, table, 'category' + categoryId)
        } else {
          table.rows(function(id, data, node) {
            return data.research_id ? true : false
          }).select()
        }
      }

      $(`#category${categoryId}_filter`).addClass('pull-right')
    },
    language: dataTablePortuguese,
    select: action !== 'show' ? {
      style: 'multi',
      selector: 'td:first-child'
    } : false,
  })
  table.on('deselect', function(e, dt, type, indexes) {
    var rowData = table.rows(0).data().toArray()
    $(`#check_all_companies_from_category${rowData[0].category_id}`)
      .prop('checked', false)
      .removeAttr('data-unchanged')
      
    $(`#check_all`).prop('checked', false)
  })
})

$('[id^=check_all_companies_from_category]').on('change', function(){
  var idTargets = $(this).attr('id').match(/\d+/)[0]
  var table = $('#category' + idTargets).DataTable()
  $(this).removeAttr('data-unchanged')
  if(this.checked){
    table.rows().select()
  } else {
    table.rows().deselect()
    $('#check_all').prop('checked', this.checked)
  }
})

$('#check_all').on('change', function() {
  $('.category_checkbox').prop('checked', this.checked).change()
})

$('[id$=company_id]').on('change', function(){
  var idFlag = $(this).val()
  $('.companies_flag_' + idFlag).prop('value', this.checked)
})

$('#research_start_at').on('change', function(){
  $('#research_end_at').prop("value", null).datepicker('setStartDate', $(this).val())
})

$('form').submit(function(e) {
  e.preventDefault()

  var categoryForm = {}
  var unchangedValues = []
  $('[id^=check_all_companies_from_category][data-unchanged="true"]').
    each((index, htmlElement) => {
      unchangedValues.push(htmlElement.id.replace('check_all_companies_from_category', ''))
    })

  $("table[id^='category']").each((index, categoryTable) => {
    var table = $('#' + categoryTable.id).DataTable()
    var tableData = table.rows({selected: true}).data().toArray()

    categoryForm[categoryTable.id] = tableData.map(rowData => {
      return {
        "research_id": rowData.research_id,
        "company_id": rowData.company_id
      }
    })
  })

  $('input#tableData').val(JSON.stringify(categoryForm)).data('companies')
  $('input#unchanged-values').val(JSON.stringify(unchangedValues)).data('unchanged_values')

  $('form').off('submit').submit();
})

$(function() {
  if(action !== "show" && !$.isEmptyObject(tableData)) {
    if(tableData.select_all == 1) {
      $('#check_all').prop("checked",true).change()
    } else {
      tableData.categories.forEach(categoryId => {
        $(`#check_all_companies_from_category${categoryId}`).prop('checked', true)
      })
    }
  }
})

function tableTemplate(categoryId) {
  return `<table class="table dataTable" role="grid" id="category${categoryId}" style="width:100%">
    <thead>
      <tr role="row">
        <th class="col-sm-1"></th>
        <th class="col-sm-4">${tradeNameTranslate}</th>
        <th class="col-sm-3">${cnpjTranslate}</th>
        <th class="col-sm-4">${emailTranslate}</th>
      </tr>
    </thead>
    <tbody>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tbody>
  </table>`
}

function fillTable(tableData, table, tableId) {
  var selectedCompanies = JSON.parse(tableData.companies)[tableId]

  if(selectedCompanies) {
    var selectedRows = selectedCompanies.map(row => row.company_id)
    table.rows((id, data, node ) => selectedRows.includes(data.company_id))
      .select()
  }
}
