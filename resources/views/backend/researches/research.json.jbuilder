json.research do
  json.id @research.id
  json.name @research.name
  json.start_at I18n.l(@research.start_at, format: :default)
  json.end_at I18n.l(@research.end_at, format: :default)
end
