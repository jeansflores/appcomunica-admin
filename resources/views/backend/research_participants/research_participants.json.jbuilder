json.data do
  json.array!(@companies) do |company|
    json.category_id company['category_id']
    json.research_id company['research_id']
    json.company_id company['id']
    json.trade_name company['trade_name']
    json.cnpj company['cnpj']
    if company.class == Hash
      json.email company['email']
    else
      json.email company&.contact&.email
    end
  end
end
