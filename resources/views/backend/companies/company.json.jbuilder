json.array!(@companies) do |company|
  json.id company.id
  json.trade_name company.trade_name
  json.cnpj company.cnpj

  json.contact do
    json.email company&.contact&.email
  end

  json.address do
    json.street company&.address&.street
  end
end
